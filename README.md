# Pseudomonas Genome Properties
Code repository for the following article: Classification of the plant-associated lifestyle of Pseudomonas strains using genome properties and machine learning.

## Usage
R script, analysis.R, contains the codes to replicate the analysis.

## Publication
Poncheewin, W., van Diepeningen, A.D., van der Lee, T.A.J. et al. 
"Classification of the plant-associated lifestyle of Pseudomonas strains using genome properties and machine learning."
Sci Rep 12, 10857 (2022). 
https://doi.org/10.1038/s41598-022-14913-4

## License
This work is licensed under the MIT license.
